import toml

class Config:
    def __init__(self, config_path: str) -> None:
        self.config_path = config_path
        self.data = self._load_config()
    
    def _load_config(self):
        with open(self.config_path_path, "r") as config_file:
            config_data = toml.load(config_data)
        return config_data


        