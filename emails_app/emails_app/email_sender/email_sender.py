import smtplib
import ssl
from pathlib import Path
from typing import Callable
class EmailSender:
    def __init__(self, sender_email, sender_password, config_path):
        self.sender_email = sender_email
        self.sender_password = sender_password
        self.smtp_server = "smtp.gmail.com"
        self.smtp_port = 465

    def send_email(self, to_email: str, subject: str, body: str) -> str:
        """
        Sends an email to the specified recipient.

        Args:
            to_email (str): The email address of the recipient.
            subject (str): The subject of the email.
            body (str): The body of the email.

        Returns:
            str: A status message indicating the success or failure of the email sending process.
        """
        message = self._create_message(subject, body)
        context = self._create_ssl_context()
        server = self._connect_to_smtp_server(context)
        self._authenticate(server)
        status = self._send_email(server, to_email, message)
        server.quit()
        return status

    def _create_message(self, subject:  str, body: str) -> str:
        """
        Creates the email message.

        Args:
            subject (str): The subject of the email.
            body (str): The body of the email.

        Returns:
            str: The formatted email message.
        """
        return f"Subject: {subject}\n\n{body}"

    def _create_ssl_context(self) -> Callable:
        """
        Creates the SSL context for secure connection.

        Returns:
            ssl.SSLContext: The SSL context.
        """
        return ssl.create_default_context()

    def _connect_to_smtp_server(self, context) -> smtplib.SMTP_SSL:
        """
        Connects to the SMTP server.

        Args:
            context (ssl.SSLContext): The SSL context.

        Returns:
            smtplib.SMTP_SSL: The SMTP server connection.
        """
        server = smtplib.SMTP_SSL(self.smtp_server, self.smtp_port, context=context)
        return server

    def _authenticate(self, server: smtplib.SMTP_SSL):
        """
        Authenticates the sender with the SMTP server.

        Args:
            server (smtplib.SMTP_SSL): The SMTP server connection.
        """
        server.login(self.sender_email, self.sender_password)

    def _send_email(self, server: smtplib.SMTP_SSL, to_email: str, message: str) -> str:
        """
        Sends the email.

        Args:
            server (smtplib.SMTP_SSL): The SMTP server connection.
            to_email (str): The email address of the recipient.
            message (str): The formatted email message.

        Returns:
            str: A status message indicating the success or failure of the email sending process.
        """
        try:
            server.sendmail(self.sender_email, to_email, message)
            return "Email sent successfully"
        except Exception as e:
            return f"Failed to send email: {str(e)}"
    
    def _get_file_names_in_folder(self, folder_path: Path):
        """This method get all the name of the client

        Args:
            folder_path (Path): path of the folder

        Raises:
            ValueError: if path is not valid

        Returns:
            _type_: _description_
        """
        folder_path = Path(folder_path)

        if not folder_path.is_dir():
            raise ValueError(f"{folder_path} n'est pas un dossier valide.")
        
        file_names = [file.name for file in folder_path.glob('*') if file.is_file()]

        return file_names