import pytest
import smtplib
from emails_app.email_sender.email_sender import EmailSender

# Remplacez ces valeurs par vos informations de test
SENDER_EMAIL = "votre@gmail.com"
SENDER_PASSWORD = "votre_mot_de_passe_d_application"
TO_EMAIL = "destinataire@example.com"
SUBJECT = "Test d'envoi d'e-mail avec Python"
BODY = "Ceci est un exemple d'e-mail envoyé depuis Python."

# Email is sent successfully with valid parameters
def test_email_sent_successfully(mocker):
    # Mock the necessary dependencies
    mocker.patch('smtplib.SMTP_SSL')
    mocker.patch('ssl.create_default_context')

    # Create an instance of EmailSender
    email_sender = EmailSender("sender@gmail.com", "password")

    # Call the send_email method with valid parameters
    result = email_sender.send_email("recipient@gmail.com", "Test Subject", "Test Body")

    # Assert that the result is the expected status message
    assert result == "Email sent successfully"


# Email is not sent when sender email is invalid
def test_invalid_sender_email(mocker):
    # Mock the necessary dependencies
    smtp_mock = mocker.patch('smtplib.SMTP_SSL')
    mocker.patch('ssl.create_default_context')

    # Create an instance of EmailSender with an invalid sender email
    email_sender = EmailSender("invalid_email", "password")

    def send_email_side_effect(*args, **kwargs):
        raise Exception("Invalid sender email address")

    smtp_mock.return_value.sendmail.side_effect = send_email_side_effect
    
    # Call the send_email method
    result = email_sender.send_email("recipient@gmail.com", "Test Subject", "Test Body")
    print("Le résultat: ", result)

    # Assert that the result is the expected status message
    assert result == "Failed to send email: Invalid sender email address"

# Email is not sent when recipient email is invalid
def test_invalid_recipient_email(mocker):
    # Mock the necessary dependencies
    smtp_mock = mocker.patch('smtplib.SMTP_SSL')
    mocker.patch('ssl.create_default_context')

    # Create an instance of EmailSender
    email_sender = EmailSender("sender@gmail.com", "password")

    # Define a custom side effect for the sendmail method
    def sendmail_side_effect(*args, **kwargs):
        raise smtplib.SMTPRecipientsRefused("Invalid recipient email address")

    # Apply the custom side effect to the sendmail method
    smtp_mock.return_value.sendmail.side_effect = sendmail_side_effect

    # Call the send_email method with an invalid recipient email
    result = email_sender.send_email("invalid_email", "Test Subject", "Test Body")

    # Assert that the result is the expected status message
    assert result == "Failed to send email: Invalid recipient email address"